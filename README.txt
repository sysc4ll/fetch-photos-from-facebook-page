Plugin WordPress (https://wppb.me/) permettant de télécharger les photos d'un album d'une page Facebook en utilisant l'API de Facebook.

J'ai eu besoin de créer ce plugin WordPress pour une assocation de protection animal.
Les photos téléchargées depuis Facebook étaient des chats à adopter.

Le téléchargement des photos ce fait tout seul et une fois par jour grâce à un "cron job".

Le code source est déposé ici pour pouvoir peut-être aider un développeur cherchant à réaliser quelque chose de similaire.
